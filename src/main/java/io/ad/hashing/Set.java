package io.ad.hashing;

public interface Set<E> {

    boolean add(E e);

    boolean contains(E e);

    boolean remove(E e);

    int size();
    
}