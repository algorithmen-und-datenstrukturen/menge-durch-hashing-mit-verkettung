package io.ad.hashing;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class HashSet<E> implements Set<E> {

    private static final double LOAD_FACTOR = 0.75;
    private static final int RESIZE_FACTOR = 2;
    private ArrayList<LinkedList<E>> hashtable;
    private int size = 0;
    
    public HashSet() {
        this(16);
    }

    public HashSet(int initialCapacity) {
        int m = computeSizeOfHashtable(initialCapacity);
        System.out.println("m="+m +" (initC="+initialCapacity+")");
        hashtable = new ArrayList<>();
        for (int i=0; i<m;i ++) {
            hashtable.add(new LinkedList<>());
        }
    }

    private int computeSizeOfHashtable(int initialCapacity) {
        double log = Math.floor(Math.log(initialCapacity)/Math.log(2));//Logarithmus von initialCapacity zur Basis 2 berechnen und nach oben runden.
        int lowerBound = (int)Math.pow(2,log);//Berechnung der nächstkleineren Zweierpotenz von initialCapacity
        int higherBound = (int)Math.pow(2,log+1);//Berechnen der nächsthöheren Zweierpotenz
        int nexthigherBound = (int)Math.pow(2,log+2);//Berechnen der nächsthöheren Zweierpotenz
        //Compute starting point between two power of two numbers. 
        //If initialCapacity is too close to the higher bound, use next higher bound.
        int start = (lowerBound+higherBound)/2;
        if (start<initialCapacity)
            start=(higherBound+nexthigherBound)/2;
        //Compute prime next to starting point
        for (int i = start;; i++) {
            if (isPrime(i)) {
                return i;
            }
        }
    }

    private int hashFunction(int key) {
        return key % hashtable.size();
    }

    private boolean isPrime(long n) {
        // check if n is a multiple of 2
        if (n % 2 == 0)
            return false;
        // if not, then just check the odds
        for (int i = 3; i * i <= n; i += 2) {
            if (n % i == 0)
                return false;
        }
        return true;
    }

    private void rehashing() {
        //Rescue elements from old hashtable
        LinkedList<E> temp = new LinkedList<>();
        for (LinkedList<E> ll : hashtable) {
            if (ll!=null) {
                temp.addAll(ll);
            }
        }
        //Resize and init hashtable
        int m = computeSizeOfHashtable(RESIZE_FACTOR*size());
        System.out.println("Rehashing:"+size()+"->"+m );

        hashtable.clear();
        for (int i=0; i<m;i++) {
            hashtable.add(new LinkedList<>());
        }
        size=0;
        
        //Add elements to new internal hashtable
        for (E elem : temp) {
            add(elem);
        }
        
    }

    @Override
    public boolean add(E e) {
        
        int pos = hashFunction(e.hashCode());
        List<E> list = hashtable.get(pos);
        if (list.contains(e)) {
            return false;
        } else {
            size++;
            list.add(0,e);
            double loadFactor = (double)size/(double)hashtable.size();
            if (loadFactor>LOAD_FACTOR) {
                System.out.println("Rehash"+loadFactor);
                rehashing();
            }
            return true;
        }
    }

    @Override
    public boolean contains(E e) {
        int pos = hashFunction(e.hashCode());
        List<E> list = hashtable.get(pos);
        return list.contains(e);
    }

    @Override
    public boolean remove(E e) {
        int pos = hashFunction(e.hashCode());
        List<E> list = hashtable.get(pos);
        boolean ok = list.remove(e);
        if (ok) size--;
        return ok;
    }

    @Override
    public int size() {
        return size;
    }    

    @Override
    public String toString() {
        return "HashSet [hashtable=" + hashtable + "]";
    }
}